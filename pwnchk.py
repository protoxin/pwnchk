#!/usr/bin/env python3

from time import sleep
try:
    import requests
except:
    print("[+]Error: Could not import requests. Is it installed??")
    exit()
import json
import sys

if len(sys.argv[1]) <= 1:
    print("Usage: ./pwn_check.py inputfile.txt")
    exit()

with open(sys.argv[1]) as f:
    for line in f.readlines():
        email = line.rstrip()
        headers = {'User-Agent':'ProToxinPWNCHK_v.03'}
        req = requests.get('https://haveibeenpwned.com/api/v2/breachedaccount/'+email,headers=headers)
        try:
            data = json.loads(req.text)
        except:
            print('[+]No breach detected for: ', email)
            sleep(3) # sleep for a few extra seconds so we don't just hammer accounts.
            continue
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        print("~~~~~~~~~~~~~~~BREACHED ACCOUNT~~~~~~~~~~~~~~~~~~~")
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        print("ACCOUNT: ",email)
        for i in data:
            print(i['Title']," - ", i['BreachDate'])
            for x in i['DataClasses']:
                print(x)
            print("\n")

        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        sleep(3) #sleep a few seconds between requests.
